"""
Sentiment Analysis model module
By Viktor Melnyk
University of Lodz
"""

import re
import joblib
import logging
import nltk

from nltk.stem import WordNetLemmatizer

from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


logging.basicConfig(level=logging.DEBUG)


class SAModel():
    """
    A simple Sentiment Analysis demonstration model
        Model uses regex to clean up unneeded characters and tags.
        Model uses WordNetLemmatizer from nltk(Natural Language Tool Kit) package to get roots more accurately.
        Model uses Vectorizer class with ngram range from 1 to 3,
            which means we are considering single words, pairs of words and triplets of words.
        Vectorizer class used is a CountVectorizer from Scikit-Learn Python package,
            that helps to "Convert a collection of text documents to a matrix of token counts"
        Model used is a LinearSVC from Scikit-Learn Python package,
            which is a "Linear Support Vector Classification Model". It is similar to SVC with parameter kernel=’linear’.
        To save and load models a joblib Python package is used to pickle models, save into file and load from file.
    """
    def __init__(self):
        """
        Initializing variables of class with empty values, compiling regex patterns
        """
        # downloading wordnet for Natural Language Tool Kit
        nltk.download('wordnet')

        # initialize class variables
        self.lemmatizer = None

        self.vectorized = None

        self.trained_model = None

        # compiling regex patterns for cleaning data
        self.NO_SPACE_REPLACE_PATTERN = re.compile(
            r'(\.)|(\,)|(\;)|(\:)|(\!)|(\?)|(")|(\()|(\))|(\[)|(\])|(\*)|(\d+)')
        self.SPACE_REPLACE_PATTERN = re.compile(
            r'(<br\s*/><br\s*/>)|(\-)|(\/)')

    def read_data(
        self,
        training_set_path='data/train.txt',
        test_set_path='data/test.txt'
    ):
        """
        Data reading method.
        Data has to be organized into two txt files,
            where every utterance has to be starting from a new line.
        """
        training_set = []
        test_set = []
        try:
            # read data from file into training set
            for line in open(training_set_path, 'r'):
                training_set.append(line.strip())

            # read data from file into test set
            for line in open(test_set_path, 'r'):
                test_set.append(line.strip())
        except Exception as e:
            raise Exception(e)
        return {
            'training_set': training_set,
            'test_set': test_set
        }

    def preprocess(self, training_set, test_set, lemmatize=True):
        """
        Method for simple data preprocessing with regex patterns.
        To get true roots of words we can set lemmatize parameter to True.
        """
        # clean up training set with regex patterns
        training_set = [
            self.NO_SPACE_REPLACE_PATTERN.sub('', line.lower()) for line in training_set
        ]
        training_set = [
            self.SPACE_REPLACE_PATTERN.sub(' ', line) for line in training_set
        ]

        # clean up test set with regex patterns
        test_set = [
            self.NO_SPACE_REPLACE_PATTERN.sub('', line.lower()) for line in test_set
        ]
        test_set = [
            self.SPACE_REPLACE_PATTERN.sub(' ', line) for line in test_set
        ]

        if lemmatize:
            # set lemmatizer
            self.lemmatizer = WordNetLemmatizer()

            # apply lemmatizer to get true roots of words in training set
            training_set = [
                ' '.join([
                    self.lemmatizer.lemmatize(word) for word in review.split()
                ]) for review in training_set
            ]

            # apply lemmatizer to get true roots of words in test set
            test_set = [
                ' '.join([
                    self.lemmatizer.lemmatize(word) for word in review.split()
                ]) for review in test_set
            ]

        return {
            'training_set': training_set,
            'test_set': test_set
        }

    def train(self, intermediate_tests=False):
        """
        Method that vectorizes data, makes data split, fits data to our model.
        If intermediate_tests parameter is set to True accuracy results for different values of penalty parameter C are displayed.
            (values for penalty parameter c are from 0.01 to 1 with step of 0.01)
        """
        # since our data is organized in a way that first 12500 items have positive grading and last 12500 have negative grading
        target = [1 if i < 12500 else 0 for i in range(25000)]
        
        raw_data = self.read_data()
        data_ = self.preprocess(raw_data['training_set'], raw_data['test_set'])

        training_set = data_['training_set']
        test_set = data_['test_set']

        if training_set and test_set:
            # initialize vectorizer
            self.vectorized = CountVectorizer(
                binary=True,
                ngram_range=(1, 3))

            # fit vectorizer to our data
            self.vectorized.fit(training_set)

            # vectorize training set
            X = self.vectorized.transform(training_set)

            # vectorize test set
            X_test = self.vectorized.transform(test_set)

            # make split of data
            X_train, X_val, y_train, y_val = train_test_split(
                X, target,
                train_size=0.75)

            if intermediate_tests:
                # display accuracy of model with different penalty coefficient c
                for c in [x * 0.01 for x in range(1, 101)]:
                    intermediate_model = LinearSVC(C=c, max_iter=50000)
                    intermediate_model.fit(X_train, y_train)
                    logging.info(
                        'Coefficient c={}, Accuracy = {}'.format(c, accuracy_score(
                            self.y_val, intermediate_model.predict(X_val))))

            # initialize Support Vector Classifier
            self.trained_model = LinearSVC(max_iter=50000)

            # fit training set (train our model)
            self.trained_model.fit(X, target)

            # log the accuracy score of our trained model evaluated on test set
            logging.info(
                'SVM Model Accuracy: {}'.format(
                    accuracy_score(target, self.trained_model.predict(X_test))))

    def evaluate(self, text):
        """
        Utility function to classify text.
        """
        return self.trained_model.predict(self.vectorized.transform([text]))

    def save_trained_model(self, filename='model.pkl'):
        """
        Utility function to save trained model into a file
        """
        if self.trained_model:
            joblib.dump(self, filename)
        else:
            logging.error('No trained model to save...')

    @staticmethod
    def load_model(filename='model.pkl'):
        """
        Utility function to load trained model from a file
        """
        try:
            return joblib.load(filename)
        except Exception as e:
            logging.error(f'Error in reading and applying featureset: {e}')
