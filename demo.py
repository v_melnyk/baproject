# import logging
import sys

from PyQt5 import QtWidgets

from interfaces.main_window import Ui_MainWindow
from interfaces.file_dialog import Ui_Dialog as Ui_File_Dialog
from interfaces.about import Ui_Dialog as Ui_About_Dialog

from models_module.SAModel import SAModel


class MainWindow(Ui_MainWindow):

    def __init__(self):
        self.valsues_map = ('Negative', 'Positive')

        self.window = QtWidgets.QMainWindow()
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self.window)
        self.window.show()

        self.model = None

        self.about = AboutWindow()
        self.file_dialog = FileWindow(self)
        self.error_dialog = QtWidgets.QErrorMessage()
        # self.info = QtWidgets.QMessageBox()
        # self.info.setIcon(QtWidgets.QMessageBox.Information)

        self.actionAbout.triggered.connect(self.call_about)
        self.actionLoad_Classification_Model.triggered.connect(self.load_from_file)
        self.actionSave_model.triggered.connect(self.save_into_file)
        self.actionTrain_Classification_Model.triggered.connect(self.train)
        self.button_analyze.clicked.connect(self.analyze)

    def train(self):
        # self.info.setText("Classification Model is being trained...")
        # self.info.setInformativeText("This might take some time...")
        # self.info.show()
        try:
            self.model = SAModel()
            self.model.train(intermediate_tests=False)
            # self.info.close()
            # self.info.setText("Trained!")
            # self.info.setInformativeText(None)
            # self.info.setStandardButtons(QtWidgets.QMessageBox.Ok)
            self.actionSave_model.setEnabled(True)
            self.groupBox_2.setEnabled(True)
        except Exception as e:
            self.error_dialog.showMessage(str(e))

    def analyze(self):
        score = self.model.evaluate(self.text_input.toPlainText())
        self.label_result.setText(self.valsues_map[score[0]])

    def call_about(self):
        self.about.window.show()

    def save_into_file(self):
        self.file_dialog.window.show()
        self.file_dialog.window.exec_()
        self.info.setText("Classification Model is being saved...")
        # self.info.setInformativeText("This might take some time...")
        # self.info.show()
        try:
            self.model.save_trained_model(self.file_dialog.file_input.text())
            # self.info.close()
            # self.info.setText("Saved!")
            # self.info.setInformativeText(None)
            # self.info.setStandardButtons(QtWidgets.QMessageBox.Ok)
        except Exception as e:
            self.error_dialog.showMessage(str(e))

    def load_from_file(self):
        self.file_dialog.window.show()
        self.file_dialog.window.exec_()
        # self.info.setText("Classification Model is being loaded...")
        # self.info.setInformativeText("This might take some time...")
        # self.info.show()
        try:
            self.model = SAModel.load_model(self.file_dialog.file_input.text())
            # self.info.close()
            # self.info.setText("Loaded!")
            # self.info.setInformativeText(None)
            # self.info.setStandardButtons(QtWidgets.QMessageBox.Ok)
            self.actionSave_model.setEnabled(True)
            self.groupBox_2.setEnabled(True)
        except Exception as e:
            self.error_dialog.showMessage(str(e))


class AboutWindow(Ui_About_Dialog):
    def __init__(self, parent=None):
        self.window = QtWidgets.QDialog()
        super(Ui_About_Dialog, self).__init__()
        self.setupUi(self.window)


class FileWindow(Ui_File_Dialog):
    def __init__(self, parent=None):
        self.window = QtWidgets.QDialog()
        super(Ui_File_Dialog, self).__init__()
        self.setupUi(self.window)

        self.button_start.clicked.connect(self.send)

    def send(self):
        self.window.close()



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ui = MainWindow()
    sys.exit(app.exec_())
