# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_window.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(335, 400)
        MainWindow.setMinimumSize(QtCore.QSize(335, 400))
        MainWindow.setMaximumSize(QtCore.QSize(335, 400))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 270, 311, 101))
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_result = QtWidgets.QLabel(self.groupBox)
        self.label_result.setText("")
        self.label_result.setAlignment(QtCore.Qt.AlignCenter)
        self.label_result.setObjectName("label_result")
        self.horizontalLayout.addWidget(self.label_result)
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setEnabled(False)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 0, 311, 261))
        self.groupBox_2.setObjectName("groupBox_2")
        self.text_input = QtWidgets.QTextEdit(self.groupBox_2)
        self.text_input.setGeometry(QtCore.QRect(10, 30, 291, 171))
        self.text_input.setObjectName("text_input")
        self.button_analyze = QtWidgets.QPushButton(self.groupBox_2)
        self.button_analyze.setGeometry(QtCore.QRect(10, 210, 141, 41))
        self.button_analyze.setObjectName("button_analyze")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 335, 22))
        self.menubar.setObjectName("menubar")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuOptions = QtWidgets.QMenu(self.menubar)
        self.menuOptions.setObjectName("menuOptions")
        MainWindow.setMenuBar(self.menubar)
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionLoad_Classification_Model = QtWidgets.QAction(MainWindow)
        self.actionLoad_Classification_Model.setObjectName("actionLoad_Classification_Model")
        self.actionTrain_Classification_Model = QtWidgets.QAction(MainWindow)
        self.actionTrain_Classification_Model.setObjectName("actionTrain_Classification_Model")
        self.actionSave_model = QtWidgets.QAction(MainWindow)
        self.actionSave_model.setEnabled(False)
        self.actionSave_model.setObjectName("actionSave_model")
        self.menuHelp.addAction(self.actionAbout)
        self.menuOptions.addAction(self.actionTrain_Classification_Model)
        self.menuOptions.addAction(self.actionLoad_Classification_Model)
        self.menuOptions.addSeparator()
        self.menuOptions.addAction(self.actionSave_model)
        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "Result"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Please enter Review text below : "))
        self.button_analyze.setText(_translate("MainWindow", "Analyze"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.menuOptions.setTitle(_translate("MainWindow", "Options"))
        self.actionAbout.setText(_translate("MainWindow", "About"))
        self.actionLoad_Classification_Model.setText(_translate("MainWindow", "Load Classification Model"))
        self.actionTrain_Classification_Model.setText(_translate("MainWindow", "Train Classification Model"))
        self.actionSave_model.setText(_translate("MainWindow", "Save model"))


