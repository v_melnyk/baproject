# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'file_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(300, 100)
        Dialog.setMinimumSize(QtCore.QSize(300, 100))
        Dialog.setMaximumSize(QtCore.QSize(300, 100))
        self.file_input = QtWidgets.QLineEdit(Dialog)
        self.file_input.setGeometry(QtCore.QRect(50, 10, 241, 31))
        self.file_input.setText("")
        self.file_input.setObjectName("file_input")
        self.input_label = QtWidgets.QLabel(Dialog)
        self.input_label.setGeometry(QtCore.QRect(10, 10, 41, 31))
        self.input_label.setObjectName("input_label")
        self.button_start = QtWidgets.QPushButton(Dialog)
        self.button_start.setGeometry(QtCore.QRect(50, 50, 131, 41))
        self.button_start.setObjectName("button_start")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.input_label.setText(_translate("Dialog", "File : "))
        self.button_start.setText(_translate("Dialog", "Proceed"))


