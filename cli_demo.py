"""
Utility Python file to demonstrate the lassification model
"""

import logging
from models_module.SAModel import SAModel


logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    valsues_map = ('"Negative"', '"Positive"')
    choice = int(input('To train new model select please enter "1", to load from .pkl file enter 2. To train model and save into .pkl file enter 0: '))
    if choice == 1:
        model = SAModel()
        model.train(intermediate_tests=False)
        while True:
            score = model.evaluate(input('Please enter your review here: '))
            print('Giver review is {}...'.format(valsues_map[score[0]]))
    elif choice == 0:
        filename = input('Enter filename (leave ampty for "model.pkl"): ')
        model = SAModel()
        model.train(intermediate_tests=input('Run intermediate tests? [y-yes]: ]') == 'y')
        if not filename == '':
            model.save_trained_model(filename)
        else:
            model.save_trained_model()
        logging.info('Model saved')
    elif choice == 2:
        filename = input('Enter filename (leave ampty for "model.pkl"): ')
        if not filename == '':
            model = SAModel.load_model(filename)
        else:
            model = SAModel.load_model()
        while True:
            score = model.evaluate(input('Please enter your review here: '))
            print('Giver review is {}...'.format(valsues_map[score[0]]))
    else:
        logging.error('Invalid choice...')
