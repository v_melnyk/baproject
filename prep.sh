gunzip -c aclImdb_v1.tar.gz | tar xopf -

mkdir data

cd aclImdb

for split in train test;
do
    for sentiment in pos neg;
    do
        for file in $split/$sentiment/*;
        do
            cat $file >> ../data/${split}.txt;
            echo >> ../data/${split}.txt;

            contents=$(<$file);
            grade=$( echo $file | cut -d '_' -f 2 | cut -d "." -f 1 )
            echo "${grade} - ${contents}" >> ../data/${split}_with_grading.txt;
        done;
    done;
done;